﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="BulkValidate.aspx.cs" Inherits="BulkValidator.BulkValidate" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    
     <div class="row">
         <div class="col-md-6">
             <h2>NFIU Bulk Validator</h2>
    
  <div class="form-group">
    <label for="inputAddress">Choose Validator</label>
   
      <asp:FileUpload ID="schema" runat="server" CssClass="form-control" />
  </div>
  <div class="form-group">
    <label for="inputAddress2">Choose XML Files</label>
    <asp:FileUpload ID="FileUpload1" runat="server" CssClass="form-control" AllowMultiple="true" />
  </div>
  
  
  <button type="submit" class="btn btn-primary" runat="server" id="btnValidate" onserverclick="btnValidate_ServerClick">Validate</button>
             </div>
         <div class="col-md-12">
             <div class="table-responsive">
             <asp:GridView ID="GridView1" runat="server" CssClass="table table-hover table-condensed table-stripped"></asp:GridView>
                 </div>
         </div>
</div>
</asp:Content>
