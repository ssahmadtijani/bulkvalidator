﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Schema;

namespace BulkValidator
{
    public class ResponseModel
    {

        public string filename { get; set; }
        public string status { get; set; }
        public string ErrorDescription { get; set; }
    }
    public partial class BulkValidate : System.Web.UI.Page
    {
        
        List<ResponseModel> model; 
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnValidate_ServerClick(object sender, EventArgs e)
        {
            var xmlFilePath = Server.MapPath("~/xmlFiles/");
            var xmlSchemaPath = Server.MapPath("~/xmlSchema/");

            model = new List<ResponseModel>();
            
            DirectoryInfo di = new DirectoryInfo(xmlFilePath);
            DirectoryInfo schemaDir = new DirectoryInfo(xmlSchemaPath);
            if (schema.HasFile)
            {

                foreach (FileInfo file in schemaDir.GetFiles())
                {
                    file.Delete();
                }
                foreach (DirectoryInfo dir in schemaDir.GetDirectories())
                {
                    dir.Delete(true);
                }

                schema.SaveAs(xmlSchemaPath + schema.FileName);
            }
            if (FileUpload1.HasFile || FileUpload1.HasFiles)
            {


                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }
                foreach (DirectoryInfo dir in di.GetDirectories())
                {
                    dir.Delete(true);
                }
                foreach (HttpPostedFile item in FileUpload1.PostedFiles)
                {
                    
                    item.SaveAs(xmlFilePath + item.FileName);
                    if (Path.GetExtension(item.FileName).Contains("zip"))
                    {
                        try
                        {




                            string zipPath = xmlFilePath +item.FileName;
                            string extractPath = xmlFilePath + item.FileName.Replace(".zip", "");

                            ZipFile.ExtractToDirectory(zipPath, extractPath);
                        }
                        catch (Exception ex)
                        {

                            throw ex;
                        }
                    }
                }
            }

            foreach (FileInfo item in di.GetFiles("*.xml", SearchOption.AllDirectories))
            {
                ResponseModel imodel = new ResponseModel();
                imodel.filename = item.FullName;
                try
                {

                    IEnumerable<string> files = Directory.EnumerateFiles(xmlSchemaPath, schema.FileName);
                    string myXMlSchemaFile = files.First();
                    

                    XmlReaderSettings readerSettings = new XmlReaderSettings();
                    readerSettings.ValidationType = ValidationType.Schema;
                    readerSettings.Schemas = new XmlSchemaSet();
                    readerSettings.Schemas.Add(null, myXMlSchemaFile);


                    using (XmlReader myXMLReader = XmlReader.Create(item.FullName, readerSettings))
                    {
                        while (myXMLReader.Read())
                        {


                        }
                    }
                }
                catch (Exception ex)
                {
                    string lastFolderName = Path.GetFileName(Path.GetDirectoryName(item.FullName));
                    imodel.filename = string.Concat(".../", lastFolderName, "/", item.Name);
                    imodel.status = "Failed";
                    imodel.ErrorDescription = (ex.Message);

                }
                if (imodel.status == "Failed")
                    model.Add(imodel);
            }

            var list = new BindingList<ResponseModel>(model);

            GridView1.DataSource = list;
            GridView1.DataBind();
            


            

        }

       
    }
}